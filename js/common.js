/*$(window).on('load', function() {
$('.firstmove').animate( { opacity: '1',}, { duration: 500, easing: 'swing', } );
});*/

$(function() {
$('.main').animate( { opacity: '1',}, { duration: 2000, easing: 'swing', } );
});

//ローディング
$(window).on('load', function() {
	$("#loading").addClass("active");
});

$(window).on('load resize', function(){
	objectFitImages('img.object-fit-img');
});

/*初期アニメーション*/
$(window).on('load resize', function(){
	$(".fanimation").each(function(index) {
		$(this).addClass("active");
	});
});

$(window).on('load resize scroll', function(){
			var value = $(window).scrollTop();
			var h = 200;
	        var w = $(window).width();
	
	//spメインビジュアル高さ指定
	//$('.maincontainer').css("height", window.innerHeight + 10);
	
	var scr_count = $(window).scrollTop() + (window.innerHeight/1.4); // ディスプレイの半分の高さ
		$(".animate").each(function(index) {
			if(scr_count > $(this).offset().top){
				$(this).addClass("active");
			}
		});	
	
	//spmodal max高さ
	$('.modalcontainer .innermodal').css("max-height", window.innerHeight - 40 );
	var mheight = $('.modalcontainer .innermodal').height();
	var wheight =window.innerHeight;
	var cposi = wheight / 2;
	var topposi = mheight / 2;
	$('.modalcontainer .closebtn').css("top",cposi - topposi );
	
	if($(window).scrollTop() > 150){
		if($(window).scrollTop() > $('body').height() - window.innerHeight - $("#footer").height() - ($(".page_top").height() / 2) ){
			var fh = $("#footer").height();
			$(".page_top").addClass("sc");
			$(".page_top").css("bottom",fh + $(".page_top").height());
		}
		else{
			$(".page_top").addClass("active");
			$(".page_top").removeClass("sc");
			$(".page_top").css("bottom","32px");
		}
	}else{
		$(".page_top").removeClass("active");
	}
	
	if($(window).scrollTop() > $('.mainbox').height()){
		$(".menu").addClass("sc");
	}else{
		$(".menu").removeClass("sc");
	}
	
		$('.gnavibox ul li a.anklink').off('click');
		$('.gnavibox ul li a.anklink').on('click', function(){
			$(".menu-trigger").toggleClass("active");
			$(".gnavibox").toggleClass("active");
			return false;
		});
	
	$('a.anklink[href*="#"]').on("click",function() {
      var speed = 1000, // ミリ秒(この値を変えるとスピードが変わる)
            href = $(this).prop("href"), //リンク先を絶対パスとして取得
            hrefPageUrl = href.split("#")[0], //リンク先を絶対パスについて、#より前のURLを取得
            currentUrl = location.href, //現在のページの絶対パスを取得
            currentUrl = currentUrl.split("#")[0]; //現在のページの絶対パスについて、#より前のURLを取得
 
        //#より前の絶対パスが、リンク先と現在のページで同じだったらスムーススクロールを実行
        if(hrefPageUrl == currentUrl){
 
            //リンク先の#からあとの値を取得
            href = href.split("#");
            href = href.pop();
            href = "#" + href;
 
            //スムースクロールの実装
            var target = $(href == "#" || href == "" ? 'html' : href),
                position = target.offset().top;
           $('body,html').animate({
                scrollTop: position
            }, speed, "easeInOutQuint", function() {
                //スムーススクロールを行ったあとに、アドレスを変更(アドレスを変えたくない場合はここを削除)
                /*if(href != "#top" && href !="#") {
                    location.href = href; 
                }*/
            });
 
            return false;
        }
 
    });
	
	//halfbox
	$(".halfbox").width($("body").width() / 2);
});

if (navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/)) {
  $(function() {
    $('.tel').each(function() {
      var str = $(this).html();
      if ($(this).children().is('img')) {
        $(this).html($('<a>').attr('href', 'tel:' + $(this).children().attr('alt').replace(/-/g, '')).append(str + '</a>'));
      } else {
        $(this).html($('<a>').attr('href', 'tel:' + $(this).text().replace(/-/g, '')).append(str + '</a>'));
      }
    });
  });
}

//SPナビ
$(window).on('load', function() {
	$(".menu-trigger").on('click', function(){
		$(".menu-trigger").toggleClass("active");
		$(".gnavibox").toggleClass("active");
		
		return false;
	});
});

//モーダルコンテンツ//
$(window).on('load', function() {
	$('.modalbtn').on('click', function(){
		$('.modalbox').hide();
		$('.modalcontainer .innermodal').scrollTop(0)
		var id = $(this).attr("id");
		$('.modalcontainer').toggleClass("active");
		$('.modalbox.' + id ).show();
		
	//spmodal max高さ
	$('.modalcontainer .innermodal').css("max-height", window.innerHeight - 40 );
	var mheight = $('.modalcontainer .innermodal').height();
	var wheight =window.innerHeight;
	var cposi = wheight / 2;
	var topposi = mheight / 2;
	$('.modalcontainer .closebtn').css("top",cposi - topposi );
		
		if($(".modalcontainer").hasClass('active')){
			var navHeight = $('.menu-trigger').innerHeight();
			$('body').css({'overflow':'hidden','height':navHeight+55});
		}
		return false;
    });
	
	$('.modalcontainer .closebtn').on('click', function(){
		$('.modalcontainer').toggleClass("active");
		$('body').css({'overflow':'auto','height':'auto'});
    });
	
	$('.modalcontainer .overray').on('click', function(){
		$('.modalcontainer').toggleClass("active");
		$('body').css({'overflow':'auto','height':'auto'});
    });
	
});
//スクロールしてトップ
$(window).on('load', function() {
	$('.page_top').on('click', function(){
        $('body,html').animate({
            scrollTop: 0
        }, 1000,"easeInOutQuint");
        return false;
    });

});